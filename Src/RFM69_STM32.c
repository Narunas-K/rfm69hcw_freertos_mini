#include "RFM69_STM32.h"
#include "RFM69_STM32_registers.h"

RFM69 RFM69Data;
RFM69InitializeData RFM69InitData;

//Sets SPI NSS pin to LOW, SET TO YOUR CONFIGURATED PIN
void RFM69_select(void){
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET); //////////////////////////////////NSS PIN//////////////////////////////////////////////////
}

//Sets SPI NSS pin to HIGH, SET TO YOUR CONFIGURATED PIN
void RFM69_unselect(void){
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET); ///////////////////////////////////////NSS PIN/////////////////////////////////////////////
	HAL_NVIC_EnableIRQ(EXTI0_IRQn); 										////////////////////////////////////////DI0 INTERRUPT PIN ////////////////////////////////////////////
}

uint8_t RFM69_readReg(uint8_t addr){
		uint8_t data=0;
		addr=addr & 0x7F;
		RFM69_select();
		HAL_SPI_Transmit(&hspi1, &addr, 1,5);
		HAL_SPI_Receive(&hspi1, &data, 1, 5);
		RFM69_unselect();
		return data;
}

void RFM69_writeReg(uint8_t addr, uint8_t value){
		addr=addr | 0x80;
		RFM69_select();
		HAL_SPI_Transmit(&hspi1, &addr, 1, 5);
		HAL_SPI_Transmit(&hspi1, &value, 1, 5);
		RFM69_unselect();
}


bool RFM69_Initialize( uint8_t freqBand, uint8_t nodeID, uint8_t networkID){



RFM69InitData._mode = RF69_MODE_STANDBY;
RFM69InitData._promiscuousMode = false;
RFM69InitData._powerLevel = 31;
RFM69InitData._isRFM69HW = false;
RFM69InitData._address = nodeID;
	
	const uint8_t CONFIG[][2] =
  {
		
    /* 0x01 */ { REG_OPMODE, RF_OPMODE_SEQUENCER_ON | RF_OPMODE_LISTEN_OFF | RF_OPMODE_STANDBY },
    /* 0x02 */ { REG_DATAMODUL, RF_DATAMODUL_DATAMODE_PACKET | RF_DATAMODUL_MODULATIONTYPE_FSK | RF_DATAMODUL_MODULATIONSHAPING_00 }, // no shaping
    /* 0x03 */ { REG_BITRATEMSB, RF_BITRATEMSB_55555}, // default: 4.8 KBPS
    /* 0x04 */ { REG_BITRATELSB, RF_BITRATELSB_55555},
    /* 0x05 */ { REG_FDEVMSB, RF_FDEVMSB_50000}, // default: 5KHz, (FDEV + BitRate / 2 <= 500KHz)
    /* 0x06 */ { REG_FDEVLSB, RF_FDEVLSB_50000},

    /* 0x07 */ { REG_FRFMSB, (uint8_t) (freqBand==RF69_315MHZ ? RF_FRFMSB_315 : (freqBand==RF69_433MHZ ? RF_FRFMSB_433 : (freqBand==RF69_868MHZ ? RF_FRFMSB_868 : RF_FRFMSB_915))) },
    /* 0x08 */ { REG_FRFMID, (uint8_t) (freqBand==RF69_315MHZ ? RF_FRFMID_315 : (freqBand==RF69_433MHZ ? RF_FRFMID_433 : (freqBand==RF69_868MHZ ? RF_FRFMID_868 : RF_FRFMID_915))) },
    /* 0x09 */ { REG_FRFLSB, (uint8_t) (freqBand==RF69_315MHZ ? RF_FRFLSB_315 : (freqBand==RF69_433MHZ ? RF_FRFLSB_433 : (freqBand==RF69_868MHZ ? RF_FRFLSB_868 : RF_FRFLSB_915))) },

    // looks like PA1 and PA2 are not implemented on RFM69W, hence the max output power is 13dBm
    // +17dBm and +20dBm are possible on RFM69HW
    // +13dBm formula: Pout = -18 + OutputPower (with PA0 or PA1**)
    // +17dBm formula: Pout = -14 + OutputPower (with PA1 and PA2)**
    // +20dBm formula: Pout = -11 + OutputPower (with PA1 and PA2)** and high power PA settings (section 3.3.7 in datasheet)
    ///* 0x11 */ { REG_PALEVEL, RF_PALEVEL_PA0_ON | RF_PALEVEL_PA1_OFF | RF_PALEVEL_PA2_OFF | RF_PALEVEL_OUTPUTPOWER_11111},
    ///* 0x13 */ { REG_OCP, RF_OCP_ON | RF_OCP_TRIM_95 }, // over current protection (default is 95mA)

    // RXBW defaults are { REG_RXBW, RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_24 | RF_RXBW_EXP_5} (RxBw: 10.4KHz)
    /* 0x19 */ { REG_RXBW, RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_16 | RF_RXBW_EXP_2 }, // (BitRate < 2 * RxBw)
    //for BR-19200: /* 0x19 */ { REG_RXBW, RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_24 | RF_RXBW_EXP_3 },
    /* 0x25 */ { REG_DIOMAPPING1, RF_DIOMAPPING1_DIO0_01 }, // DIO0 is the only IRQ we're using
    /* 0x26 */ { REG_DIOMAPPING2, RF_DIOMAPPING2_CLKOUT_OFF }, // DIO5 ClkOut disable for power saving
    /* 0x28 */ { REG_IRQFLAGS2, RF_IRQFLAGS2_FIFOOVERRUN }, // writing to this bit ensures that the FIFO & status flags are reset
    /* 0x29 */ { REG_RSSITHRESH, 220 }, // must be set to dBm = (-Sensitivity / 2), default is 0xE4 = 228 so -114dBm
    ///* 0x2D */ { REG_PREAMBLELSB, RF_PREAMBLESIZE_LSB_VALUE } // default 3 preamble bytes 0xAAAAAA
    /* 0x2E */ { REG_SYNCCONFIG, RF_SYNC_ON | RF_SYNC_FIFOFILL_AUTO | RF_SYNC_SIZE_2 | RF_SYNC_TOL_0 },
    /* 0x2F */ { REG_SYNCVALUE1, 0x2D },      // attempt to make this compatible with sync1 byte of RFM12B lib
    /* 0x30 */ { REG_SYNCVALUE2, networkID }, // NETWORK ID
    /* 0x37 */ { REG_PACKETCONFIG1, RF_PACKET1_FORMAT_VARIABLE | RF_PACKET1_DCFREE_OFF | RF_PACKET1_CRC_ON | RF_PACKET1_CRCAUTOCLEAR_ON | RF_PACKET1_ADRSFILTERING_OFF },
    /* 0x38 */ { REG_PAYLOADLENGTH, 66 }, // in variable length mode: the max frame size, not used in TX
    ///* 0x39 */ { REG_NODEADRS, nodeID }, // turned off because we're not using address filtering
    /* 0x3C */ { REG_FIFOTHRESH, RF_FIFOTHRESH_TXSTART_FIFONOTEMPTY | RF_FIFOTHRESH_VALUE }, // TX on FIFO not empty
    /* 0x3D */ { REG_PACKETCONFIG2, RF_PACKET2_RXRESTARTDELAY_2BITS | RF_PACKET2_AUTORXRESTART_ON | RF_PACKET2_AES_OFF }, // RXRESTARTDELAY must match transmitter PA ramp-down time (bitrate dependent)
    //for BR-19200: /* 0x3D */ { REG_PACKETCONFIG2, RF_PACKET2_RXRESTARTDELAY_NONE | RF_PACKET2_AUTORXRESTART_ON | RF_PACKET2_AES_OFF }, // RXRESTARTDELAY must match transmitter PA ramp-down time (bitrate dependent)
    /* 0x6F */ { REG_TESTDAGC, RF_DAGC_IMPROVED_LOWBETA0 }, // run DAGC continuously in RX mode for Fading Margin Improvement, recommended default for AfcLowBetaOn=0
    {255, 0}
  };
	RFM69_unselect();
	uint32_t start = millis();
	uint32_t timeout = 100;
	//printf(" pries pirmaa while \n");
	do RFM69_writeReg(REG_SYNCVALUE1, 0xAA); while (RFM69_readReg(REG_SYNCVALUE1) != 0xaa && millis()-start < timeout);
	//printf("po pirmo while\n");
  start = millis();
  do RFM69_writeReg(REG_SYNCVALUE1, 0x55); while (RFM69_readReg(REG_SYNCVALUE1) != 0x55 && millis()-start < timeout);
	//printf("po antro while\n");
  for (uint8_t i = 0; CONFIG[i][0] != 255; i++)
    RFM69_writeReg(CONFIG[i][0], CONFIG[i][1]);
	//printf("po for \n");
	//printf("asdjkaskdjlkasdj");
	//printf("12335432123");
	RFM69_encrypt(0);
	//printf("po encrypt \n");
	RFM69_setHighPower(RFM69InitData._isRFM69HW);
	RFM69_setMode(RF69_MODE_STANDBY);
	while (((RFM69_readReg(REG_IRQFLAGS1) & RF_IRQFLAGS1_MODEREADY) == 0x00) && millis()-start < timeout); // wait for ModeReady
  if (millis()-start >= timeout)
    return false;
	RFM69InitData._address=nodeID;
	return true;
	
}


void RFM69_setMode(uint8_t newMode){
	if (newMode == RFM69InitData._mode)
    return;
	
	switch (newMode) {
    case RF69_MODE_TX:
      RFM69_writeReg(REG_OPMODE, (RFM69_readReg(REG_OPMODE) & 0xE3) | RF_OPMODE_TRANSMITTER);
      if (RFM69InitData._isRFM69HW) RFM69_setHighPowerRegs(true);
      break;
    case RF69_MODE_RX:
      RFM69_writeReg(REG_OPMODE, (RFM69_readReg(REG_OPMODE) & 0xE3) | RF_OPMODE_RECEIVER);
      if (RFM69InitData._isRFM69HW) RFM69_setHighPowerRegs(false);
      break;
    case RF69_MODE_SYNTH:
      RFM69_writeReg(REG_OPMODE, (RFM69_readReg(REG_OPMODE) & 0xE3) | RF_OPMODE_SYNTHESIZER);
      break;
    case RF69_MODE_STANDBY:
      RFM69_writeReg(REG_OPMODE, (RFM69_readReg(REG_OPMODE) & 0xE3) | RF_OPMODE_STANDBY);
      break;
    case RF69_MODE_SLEEP:
      RFM69_writeReg(REG_OPMODE, (RFM69_readReg(REG_OPMODE) & 0xE3) | RF_OPMODE_SLEEP);
      break;
    default:
      return;
  }
	 while (RFM69InitData._mode == RF69_MODE_SLEEP && (RFM69_readReg(REG_IRQFLAGS1) & RF_IRQFLAGS1_MODEREADY) == 0x00); // wait for ModeReady
	RFM69InitData._mode=newMode;
}

//internal function
void RFM69_setHighPowerRegs(bool onOff) {
  RFM69_writeReg(REG_TESTPA1, onOff ? 0x5D : 0x55);
  RFM69_writeReg(REG_TESTPA2, onOff ? 0x7C : 0x70);
}


void RFM69_encrypt(const char* key) {
	RFM69_setMode(RF69_MODE_STANDBY);
  if (key != 0)
  {
		uint8_t value=REG_AESKEY1 | 0x80;
    RFM69_select();
		HAL_SPI_Transmit(&hspi1, &value, 1, 5);
    for (uint8_t i = 0; i < 16; i++){
			uint8_t key_var = key[i];
			HAL_SPI_Transmit(&hspi1, &key_var, 1, 5);
		}
    RFM69_unselect();
  }
  RFM69_writeReg(REG_PACKETCONFIG2, (RFM69_readReg(REG_PACKETCONFIG2) & 0xFE) | (key ? 1 : 0));
}

void RFM69_setHighPower(bool onOff) {
  RFM69InitData._isRFM69HW = onOff;
  RFM69_writeReg(REG_OCP, RFM69InitData._isRFM69HW ? RF_OCP_OFF : RF_OCP_ON);
  if (RFM69InitData._isRFM69HW) // turning ON
    RFM69_writeReg(REG_PALEVEL, (RFM69_readReg(REG_PALEVEL) & 0x1F) | RF_PALEVEL_PA1_ON | RF_PALEVEL_PA2_ON); // enable P1 & P2 amplifier stages
  else
    RFM69_writeReg(REG_PALEVEL, RF_PALEVEL_PA0_ON | RF_PALEVEL_PA1_OFF | RF_PALEVEL_PA2_OFF | RFM69InitData._powerLevel); // enable P0 only
}

//put transceiver in sleep mode to save battery - to wake or resume receiving just call receiveDone()
void RFM69_sleep(void) {
  RFM69_setMode(RF69_MODE_SLEEP);
}

//set this node's address
void RFM69_setAddress(uint8_t addr)
{
  RFM69InitData._address = addr;
  RFM69_writeReg(REG_NODEADRS, RFM69InitData._address);
}

//set this node's network id
void RFM69_setNetwork(uint8_t networkID)
{
  RFM69_writeReg(REG_SYNCVALUE2, networkID);
}

//STM32 HAL implementation of Arduino's millis
uint32_t millis(void) {
	return HAL_GetTick();
}


int16_t RFM69_readRSSI(bool forceTrigger) {
  int16_t rssi = 0;
  if (forceTrigger)
  {
    // RSSI trigger not needed if DAGC is in continuous mode
    RFM69_writeReg(REG_RSSICONFIG, RF_RSSI_START);
    while ((RFM69_readReg(REG_RSSICONFIG) & RF_RSSI_DONE) == 0x00); // wait for RSSI_Ready
  }
  rssi = -RFM69_readReg(REG_RSSIVALUE);
  rssi >>= 1;
  return rssi;
}



void RFM69_sendFrame(uint8_t toAddress, uint8_t* buffer, uint8_t bufferSize, bool requestACK, bool sendACK)
{
  RFM69_writeReg(REG_PACKETCONFIG2, (RFM69_readReg(REG_PACKETCONFIG2) & 0xFB) | RF_PACKET2_RXRESTART); // avoid RX deadlocks
	RFM69_setMode(RF69_MODE_STANDBY); // turn off receiver to prevent reception while filling fifo
  while ((RFM69_readReg(REG_IRQFLAGS1) & RF_IRQFLAGS1_MODEREADY) == 0x00); // wait for ModeReady
  RFM69_writeReg(REG_DIOMAPPING1, RF_DIOMAPPING1_DIO0_00); // DIO0 is "Packet Sent"
  if (bufferSize > RF69_MAX_DATA_LEN) bufferSize = RF69_MAX_DATA_LEN;
  // control byte
  uint8_t CTLbyte = 0x00;
  if (sendACK)
    CTLbyte = RFM69_CTL_SENDACK;
  else if (requestACK)
    CTLbyte = RFM69_CTL_REQACK;

  // write to FIFO
	uint8_t FIFO_Reg = REG_FIFO | 0x80;
	uint8_t buffSize = bufferSize + 3;
  RFM69_select();
  HAL_SPI_Transmit(&hspi1, &FIFO_Reg, 1, 5);
	HAL_SPI_Transmit(&hspi1, &buffSize, 1, 5);
  HAL_SPI_Transmit(&hspi1, &toAddress, 1, 5);
	HAL_SPI_Transmit(&hspi1, &(RFM69InitData._address), 1, 5);
	HAL_SPI_Transmit(&hspi1, &CTLbyte, 1, 5);

  for (uint8_t i = 0; i < bufferSize; i++)
		HAL_SPI_Transmit(&hspi1, &buffer[i], 1, 5);
  RFM69_unselect();

  // no need to wait for transmit mode to be ready since its handled by the radio
  RFM69_setMode(RF69_MODE_TX);
  uint32_t txStart = millis();
  while ((HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0) == 0) && ((millis() - txStart) < RF69_TX_LIMIT_MS)); //////////////////// wait for DIO0 INTERRUPT PIN to turn HIGH signalling transmission finish////////////////////////
	
	//while ((RFM69_readReg(REG_IRQFLAGS2) & RF_IRQFLAGS2_PACKETSENT )== 0x00); // wait for ModeReady
  RFM69_setMode(RF69_MODE_STANDBY);
	HAL_Delay(0);
	//printf("MODE3: %i\n", RFM69_readReg(REG_OPMODE));
}


void RFM69_receiveBegin(void) {
  RFM69Data.DATALEN = 0;
  RFM69Data.SENDERID = 0;
  RFM69Data.TARGETID = 0;
  RFM69Data.PAYLOADLEN = 0;
  RFM69Data.ACK_REQUESTED = 0;
  RFM69Data.ACK_RECEIVED = 0;
  RFM69Data.RSSI = 0;
  if (RFM69_readReg(REG_IRQFLAGS2) & RF_IRQFLAGS2_PAYLOADREADY)
    RFM69_writeReg(REG_PACKETCONFIG2, (RFM69_readReg(REG_PACKETCONFIG2) & 0xFB) | RF_PACKET2_RXRESTART); // avoid RX deadlocks
  RFM69_writeReg(REG_DIOMAPPING1, RF_DIOMAPPING1_DIO0_01); // set DIO0 to "PAYLOADREADY" in receive mode
  RFM69_setMode(RF69_MODE_RX);
}

bool RFM69_receiveDone(void) {
//ATOMIC_BLOCK(ATOMIC_FORCEON)
//{
  HAL_NVIC_DisableIRQ(EXTI0_IRQn); // re-enabled in unselect() via setMode() or via receiveBegin() ////////////////////////////////////////////DIO INTERRUPT NUMBER////////////////////////
  if ((RFM69InitData._mode == RF69_MODE_RX) && RFM69Data.PAYLOADLEN > 0)
  {
    RFM69_setMode(RF69_MODE_STANDBY); // enables interrupts
    return true;
  }
  else if (RFM69InitData._mode == RF69_MODE_RX) // already in RX no payload yet
  {
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);// explicitly re-enable interrupts    ////////////////////////////////////////////DIO INTERRUPT NUMBER////////////////////////
    return false;
  }
  RFM69_receiveBegin();
  return false;
//}
}

void RFM69_interruptHandler(void) {
  //pinMode(4, OUTPUT);
  //digitalWrite(4, 1);
  if (RFM69InitData._mode == RF69_MODE_RX && (RFM69_readReg(REG_IRQFLAGS2) & RF_IRQFLAGS2_PAYLOADREADY))
  {
    //RSSI = readRSSI();
		RFM69_setMode(RF69_MODE_STANDBY);
    RFM69_select();
    uint8_t regFifoRead = REG_FIFO & 0x7F;
		HAL_SPI_Transmit(&hspi1, &regFifoRead, 1, 5);
		uint8_t payloadLenTemp =0;
		HAL_SPI_Receive(&hspi1, &payloadLenTemp,  1, 5);
		RFM69Data.PAYLOADLEN = payloadLenTemp; 
    RFM69Data.PAYLOADLEN = RFM69Data.PAYLOADLEN > 66 ? 66 : RFM69Data.PAYLOADLEN; // precaution
		uint8_t targetIdTemp = 0;
    HAL_SPI_Receive(&hspi1, &targetIdTemp,  1, 5);
		RFM69Data.TARGETID = targetIdTemp;
    if(!(RFM69InitData._promiscuousMode || RFM69Data.TARGETID == RFM69InitData._address ||RFM69Data.TARGETID == RF69_BROADCAST_ADDR) // match this node's address, or broadcast address or anything in promiscuous mode
       || RFM69Data.PAYLOADLEN < 3) // address situation could receive packets that are malformed and don't fit this libraries extra fields
    {
      RFM69Data.PAYLOADLEN = 0;
      RFM69_unselect();
      RFM69_receiveBegin();
      //digitalWrite(4, 0);
      return;
    }
    RFM69Data.DATALEN = RFM69Data.PAYLOADLEN - 3;
    uint8_t senderIdTemp = 0;
		HAL_SPI_Receive(&hspi1, &senderIdTemp, 1, 5);
		RFM69Data.SENDERID = senderIdTemp;
		
    uint8_t CTLbyte = 0;
		HAL_SPI_Receive(&hspi1, &CTLbyte, 1, 5);

    RFM69Data.ACK_RECEIVED = CTLbyte & RFM69_CTL_SENDACK; // extract ACK-received flag
    RFM69Data.ACK_REQUESTED = CTLbyte & RFM69_CTL_REQACK; // extract ACK-requested flag
    
    //interruptHook(CTLbyte);     // TWS: hook to derived class interrupt function

    for (uint8_t i = 0; i < RFM69Data.DATALEN; i++)
    {
			uint8_t dataTemp = 0;
			HAL_SPI_Receive(&hspi1, &dataTemp, 1, 5);
			
      RFM69Data.DATA[i] = dataTemp;
    }
    if (RFM69Data.DATALEN < RF69_MAX_DATA_LEN) RFM69Data.DATA[RFM69Data.DATALEN] = 0; // add null at end of string
    RFM69_unselect();
    RFM69_setMode(RF69_MODE_RX);
		
	}
  RFM69Data.RSSI = RFM69_readRSSI(false);
  //digitalWrite(4, 0);
}

// true  = disable filtering to capture all frames on network
// false = enable node/broadcast filtering to capture only frames sent to this/broadcast address
void RFM69_promiscuous(bool onOff) {
  RFM69InitData._promiscuousMode = onOff;
  //writeReg(REG_PACKETCONFIG1, (readReg(REG_PACKETCONFIG1) & 0xF9) | (onOff ? RF_PACKET1_ADRSFILTERING_OFF : RF_PACKET1_ADRSFILTERING_NODEBROADCAST));
}

bool RFM69_ACKReceived(uint8_t fromNodeID) {
	bool ACK = false;
	if (RFM69_receiveDone()){
    if((RFM69Data.SENDERID == fromNodeID || fromNodeID == RF69_BROADCAST_ADDR) && RFM69Data.ACK_RECEIVED){
			ACK = true;
		}
		else{
			ACK = false;
		}
	}
	return ACK;
}

// check whether an ACK was requested in the last received packet (non-broadcasted packet)
bool RFM69_ACKRequested(void) {
  return RFM69Data.ACK_REQUESTED && (RFM69Data.TARGETID != RF69_BROADCAST_ADDR);
}

void RFM69_sendACK(uint8_t ACKToSend) {
	uint8_t ACKData[1] = {ACKToSend | 0x7F};
  RFM69Data.ACK_REQUESTED = 0;   // TWS added to make sure we don't end up in a timing race and infinite loop sending Acks
  uint8_t sender = RFM69Data.SENDERID;
  int16_t _RSSI = RFM69Data.RSSI; // save payload received RSSI value
  RFM69_writeReg(REG_PACKETCONFIG2, (RFM69_readReg(REG_PACKETCONFIG2) & 0xFB) | RF_PACKET2_RXRESTART); // avoid RX deadlocks
  RFM69Data.SENDERID = sender;    // TWS: Restore SenderID after it gets wiped out by receiveDone()
	HAL_Delay(20);
	//printf("Sender ID %i\n", RFM69Data.SENDERID);
  RFM69_sendFrame(sender, ACKData, 1, false, true);
  RFM69Data.RSSI = _RSSI; // restore payload RSSI
}
